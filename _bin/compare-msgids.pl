#!/usr/bin/perl -w
# Quick and dirty script to compare two msgids (when using po4a with
# --previous) with wdiff to find the differences between them

my $file1="diff1";
my $file2="diff2";

open (my $DIFF1, ">$file1") || die ("Cannot open $file1: $!");
open (my $DIFF2, ">$file2") || die ("Cannot open $file2: $!");

my $fileh="";
while ($line = <STDIN>) {
    $fileh="";
    chomp $line;
    $file = $DIFF1 if ( $line =~ /^\#\|/ ) ;
    $file = $DIFF2 if ( $line =~ /^"/ ) ;
    $line =~ s/^\#\| //;
    $line =~ s/^"//;
    $line =~ s/"$//;
    print $file $line if $line ne 'msgid "';
}

close $DIFF1; close $DIFF2;

exec ("wdiff -3 $file1 $file2 | less");

exit;
